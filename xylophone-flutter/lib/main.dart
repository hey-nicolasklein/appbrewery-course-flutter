import 'package:flutter/material.dart';
import 'package:audioplayers/audio_cache.dart';
import 'package:flutter/services.dart';


void main() => runApp(XylophoneApp());

class XylophoneApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        backgroundColor: Colors.black,
        body: AnnotatedRegion<SystemUiOverlayStyle>(
          value: SystemUiOverlayStyle.light,
          child: SafeArea(
            child: ButtonInstrument(),
          ),
        ),
      ),
    );
  }
}

class AudioButton extends StatelessWidget {
  AudioButton({Key key, this.audioNr = "1", this.color=Colors.red}) : super(key: key);

  final String audioNr;
  final MaterialColor color;

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: FlatButton(
        color: color,
        onPressed: () {
          final player = AudioCache();
          player.play('note$audioNr.wav');
        },
      ),
    );
  }
}

class ButtonInstrument extends StatelessWidget {
  @override
  Widget build(BuildContext context) {


    return Center(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          AudioButton(),
          AudioButton(audioNr: "2", color: Colors.orange),
          AudioButton(audioNr: "3", color: Colors.yellow),
          AudioButton(audioNr: "4", color: Colors.green),
          AudioButton(audioNr: "5", color: Colors.blueGrey),
          AudioButton(audioNr: "6", color: Colors.blue),
          AudioButton(audioNr: "7", color: Colors.purple),
        ],
      ),
    );
  }
}
