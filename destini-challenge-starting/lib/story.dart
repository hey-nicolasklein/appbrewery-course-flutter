class Story{
  String story_title;
  String choice_1;
  String choice_2;

  Story({storyTitle, choice1, choice2}){
    this.story_title = storyTitle;
    this.choice_1 = choice1;
    this.choice_2 = choice2;
  }
}
