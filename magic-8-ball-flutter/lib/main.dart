import 'package:flutter/material.dart';
import 'dart:math';

void main() => runApp(
      MaterialApp(
        theme: ThemeData(primaryColor: Colors.blueAccent, backgroundColor: Colors.blue),
        home: Scaffold(
          appBar: AppBar(
            title: const Text("Ask me Anything"),
          ),
          body: Ball(),
        ),
      ),
    );

class Ball extends StatefulWidget {
  @override
  _BallState createState() => _BallState();
}

class _BallState extends State<Ball> {

  int currentBallNumber = 1;

  void newBall(){
    currentBallNumber = (currentBallNumber % 6) + 1;
    currentBallNumber = Random().nextInt(5) +1;
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: FlatButton(
          child: Image.asset('images/ball$currentBallNumber.png'),
          onPressed: () {
            setState(() {
              newBall();
            });
          },
        ),
      ),
    );
  }
}

