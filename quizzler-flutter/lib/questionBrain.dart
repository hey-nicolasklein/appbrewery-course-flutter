import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'questions.dart';

class QuestionBrain{

  int _question_index = 0;

  List<Question> _questionBank = [
    Question(q: 'You can lead a cow down stairs but not up stairs.', a: false),
    Question(
        q: 'Approximately one quarter of human bones are in the feet.',
        a: true),
    Question(q: 'A slug\'s blood is green.', a: true)
  ];

  void nextQuestion(){
    if(_question_index < _questionBank.length-1){
      _question_index++;
    }
  }

  String getQuestion(){
    return _questionBank[_question_index].questionText;
  }

  bool getAnswer(){
    return _questionBank[_question_index].questionAnswer;
  }

  bool reachedLastQuestion(){
    return _question_index >= _questionBank.length-1 ? true : false;
  }

  void resetQuestionNumber(){
    _question_index = 0;
  }




}
